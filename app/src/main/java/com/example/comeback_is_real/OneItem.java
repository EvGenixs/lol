package com.example.comeback_is_real;

import com.google.gson.annotations.SerializedName;

class OneItem {
    @SerializedName("address")
    public String address;
    @SerializedName("banktype")
    public String banktype;
    @SerializedName("lat")
    public String lat;
    @SerializedName("lon")
    public String lon;
    @SerializedName("telephone")
    public String telephone;
    @SerializedName("timeend")
    public String timeend;
    @SerializedName("timestart")
    public String timestart;
    @SerializedName("type")
    public String type;

}
