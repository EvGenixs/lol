package com.example.comeback_is_real;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private List<OneItem> mDataset;
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public View view;
        public MyViewHolder(View v) {
            super(v);
            view = v;
        }
        public void setData(OneItem i){
            TextView ad;
            ad = view.findViewById(R.id.address);
            ad.setText(i.address);
            TextView ts;
            ts = view.findViewById(R.id.timestart);
            ts.setText(i.timestart);
            TextView te;
            te = view.findViewById(R.id.timeend);
            te.setText(i.timeend);
            TextView bt;
            bt = view.findViewById(R.id.banktype);
            bt.setText(i.banktype);
            TextView tel;
            tel = view.findViewById(R.id.telephone);
            tel.setText(i.telephone);
            TextView t;
            t = view.findViewById(R.id.type);
            t.setText(i.type);

        }
    }
    public MyAdapter(List<OneItem> myDataset) {
        this.mDataset = myDataset;
    }
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_one_item, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.setData((mDataset.get(position)));
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
